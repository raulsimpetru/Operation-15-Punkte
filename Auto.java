import java.awt.*;
import java.util.ArrayList;

/**
 * @author Raul Simpetru
 */
public class Auto {

    public int richtung; // 0 ist rechts; 1 ist links
    private final int y, farbe1, farbe2;
    private float x;
    public boolean stop;


    public Auto(int _x, int _y, int _farbe1, int _farbe2) {
        x = _x;
        y = _y;
        farbe1 = _farbe1;
        farbe2 = _farbe2;
        stop = false;
    }

    public void zeichne(Zeichenfenster fenster) {
        fenster.fuelleRechteck((int) x, y, 60, 40, farbe1);

        if (richtung == 0) {
            fenster.fuelleRechteck((int) x + 4, y + 5, 37, 30, farbe2);
            fenster.fuelleRechteck((int) x + 58, y + 1, 2, 5, Color.white);
            fenster.fuelleRechteck((int) x + 58, y + 34, 2, 5, Color.white);
            fenster.fuelleRechteck((int) x, y + 2, 2, 5, Color.orange);
            fenster.fuelleRechteck((int) x, y + 33, 2, 5, Color.orange);
        } else {
            fenster.fuelleRechteck((int) x + 60 - 37 - 4, y + 5, 37, 30, farbe2);
            fenster.fuelleRechteck((int) x, y + 1, 2, 5, Color.white);
            fenster.fuelleRechteck((int) x, y + 34, 2, 5, Color.white);
            fenster.fuelleRechteck((int) x + 58, y + 2, 2, 5, Color.orange);
            fenster.fuelleRechteck((int) x + 58, y + 33, 2, 5, Color.orange);
        }

    }

    public void bewege() {
        if (!stop) {
            if (richtung == 0) x += 1.65;
            if (richtung == 1) x -= 1.65;
        }
    }

    public int getFrontX() {
        return richtung == 0 ? (int) x + 60 : (int) x;
    }

    public int getBackX() {
        return richtung == 0 ? (int) x : (int) x + 60;
    }

    /**
     * @param aza abstandZwischenAutos
     */

    public void prufeAbstand(Auto _auto, ArrayList<Auto> _autoArray, int aza) {
        if (richtung == 0 && this.getFrontX() >= _auto.getBackX() - aza) {
            this.stop = true;
            if (_autoArray.indexOf(this) != _autoArray.size() - 1) {
                Auto temp = _autoArray.get(_autoArray.indexOf(this) + 1);
                if (temp != null) {
                    temp.prufeAbstand(this, _autoArray, aza);
                }
            }
        } else if (richtung == 1 && this.getFrontX() <= _auto.getBackX() + aza) {
            this.stop = true;
            if (_autoArray.indexOf(this) != _autoArray.size() - 1) {
                Auto temp = _autoArray.get(_autoArray.indexOf(this) + 1);
                if (temp != null) {
                    temp.prufeAbstand(this, _autoArray, aza);
                }
            }
        }
    }
}