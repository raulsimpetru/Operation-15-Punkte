/**
 * @author Raul Simpetru
 */
public class AmpelA extends Ampel {

    public int state = 2; //0 = rot, 1 = gelb, 2 = gruen
    private final int x, y;

    public AmpelA(int _x, int _y) {
        super();
        x = _x;
        y = _y;
    }

    @Override
    public void zeichne(Zeichenfenster fenster) {
        fenster.fuelleRechteck(x, y, 30, 80, "grau");
        wechsleLicht(fenster);
    }

    @Override
    protected void wechsleLicht(Zeichenfenster fenster) {
        switch (state) {
            case 0:
                fenster.fuelleKreis(x + 15, y + 40 - 20 - 6, 12, "rot");
                fenster.fuelleKreis(x + 15, y + 40, 12, "schwarz");
                fenster.fuelleKreis(x + 15, y + 40 + 20 + 6, 12, "schwarz");
                break;
            case 1:
                fenster.fuelleKreis(x + 15, y + 40 - 20 - 6, 12, "schwarz");
                fenster.fuelleKreis(x + 15, y + 40, 12, "gelb");
                fenster.fuelleKreis(x + 15, y + 40 + 20 + 6, 12, "schwarz");
                break;
            case 2:
                fenster.fuelleKreis(x + 15, y + 40 - 20 - 6, 12, "schwarz");
                fenster.fuelleKreis(x + 15, y + 40, 12, "schwarz");
                fenster.fuelleKreis(x + 15, y + 40 + 20 + 6, 12, "gruen");
                break;
        }
    }

    @Override
    public boolean sollAnhalten() {
        return state == 0 || state == 1;
    }
}
