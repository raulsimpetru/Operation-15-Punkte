import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Random;
import java.util.TimerTask;

/**
 * @author Raul Simpetru
 */

public class Simulation {

    private final Zeichenfenster fenster;
    private final Random random;
    private final int breite = 800, hoehe = 800;
    private final ArrayList<Auto> autoArrayL, autoArrayR; //L gehen nach Links R gehen nach Rechts;
    private final ArrayList<Fussgaenger> fussgaengerArrayU, fussgaengerArrayO; // U gehen nach Unten O gehen nach Oben;
    private final AmpelA ampelA1, ampelA2;
    private final AmpelF ampelF1, ampelF2;
    private final java.util.Timer timerUtil = new java.util.Timer();

    private javax.swing.Timer mainTimer;
    private boolean start = false;
    private int MenschenO, MenschenU, MeschenOGeloescht, MeschenUGeloescht;

    @SuppressWarnings("FieldCanBeLocal")
    private final double intervalAmpel = 1.5; // interval in seconds

    private final int abstandZwichenAutos = 20;

    public static void main(String[] args) {
        new Simulation();
    }

    private Simulation() {
        random = new Random();

        autoArrayL = new ArrayList<>();
        autoArrayR = new ArrayList<>();

        fussgaengerArrayU = new ArrayList<>();
        fussgaengerArrayO = new ArrayList<>();

        fenster = new Zeichenfenster("Simulation", breite, hoehe);

        ampelA1 = new AmpelA(breite / 2 - (100 / 2) - 10 - 30, hoehe / 2 + 80); //unten
        ampelA2 = new AmpelA(breite / 2 + (100 / 2) + 10, hoehe / 2 - 160); //oben

        ampelF1 = new AmpelF(breite / 2 + (100 / 2) + 10, hoehe / 2 + 54 + (80 - 54)); // unten
        ampelF2 = new AmpelF(breite / 2 - (100 / 2) - 10 - 30, hoehe / 2 - 75 - 54 - 5); //oben

        JButton startButton = new JButton("Start");
        startButton.addActionListener(e -> {
            if (!start)
                starten();
            else {
                mainTimer.start();
                start = true;
            }
        });

        JButton stopButton = new JButton("Stop");
        stopButton.addActionListener(e -> mainTimer.stop());

        JTextField autoText = new JTextField();
        autoText.setHorizontalAlignment(JTextField.CENTER);
        autoText.setMaximumSize(
                new Dimension(Integer.MAX_VALUE, autoText.getPreferredSize().height));

        JButton addAutoButton = new JButton("Fuege Auto(s) hinzu");
        addAutoButton.addActionListener(e -> {
            String input = autoText.getText();

            //https://www.programiz.com/java-programming/examples/check-string-numeric

            if (input.matches("-?\\d+(\\.\\d+)?") && Integer.valueOf(input) > 0) {
                autoText.setText("");
                erzeugeAutos(Integer.valueOf(input));
            }
        });

        JTextField fussgaengerText = new JTextField();
        fussgaengerText.setHorizontalAlignment(JTextField.CENTER);
        fussgaengerText.setMaximumSize(
                new Dimension(Integer.MAX_VALUE, fussgaengerText.getPreferredSize().height));

        JButton addFussgaengerButton = new JButton("Fuege Fussgänger hinzu");
        addFussgaengerButton.addActionListener(e -> {
            String input = fussgaengerText.getText();

            //https://www.programiz.com/java-programming/examples/check-string-numeric

            if (input.matches("-?\\d+(\\.\\d+)?") && Integer.valueOf(input) > 0) {
                fussgaengerText.setText("");
                Simulation.this.erzeugeMenschen(Integer.valueOf(input), true);

                int temp = Integer.valueOf(input);

                if (temp % 2 == 0) {
                    MenschenO += temp / 2;
                    MenschenU += temp / 2;

                } else if (temp % 2 != 0) {

                    if (temp == 1) MenschenU++;
                    else {
                        MenschenO += (temp - 1) / 2;
                        MenschenU += (temp - 1) / 2 + 1;
                    }
                }
            }
        });

        fenster.komponenteHinzufuegen(Box.createRigidArea(new Dimension(5, 0)), "unten"); //new shit
        fenster.komponenteHinzufuegen(startButton, "unten");
        fenster.komponenteHinzufuegen(Box.createRigidArea(new Dimension(5, 0)), "unten"); //new shit
        fenster.komponenteHinzufuegen(stopButton, "unten");
        fenster.komponenteHinzufuegen(Box.createRigidArea(new Dimension(5, 0)), "unten"); //new shit
        fenster.komponenteHinzufuegen(addAutoButton, "unten");
        fenster.komponenteHinzufuegen(Box.createRigidArea(new Dimension(5, 0)), "unten"); //new shit
        fenster.komponenteHinzufuegen(autoText, "unten");
        fenster.komponenteHinzufuegen(Box.createRigidArea(new Dimension(5, 0)), "unten"); //new shit
        fenster.komponenteHinzufuegen(addFussgaengerButton, "unten");
        fenster.komponenteHinzufuegen(Box.createRigidArea(new Dimension(5, 0)), "unten"); //new shit
        fenster.komponenteHinzufuegen(fussgaengerText, "unten");
        fenster.komponenteHinzufuegen(Box.createRigidArea(new Dimension(5, 0)), "unten"); //new shit

        ziechneStartScreen();

        erzeugeAutos(20);
        erzeugeMenschen(6, false);

    }

    private void starten() {
        ActionListener al = evt -> {
            zeichne();
            pruefeAmpel();
            pruefeAusserhalb();
            fenster.zeige();
        };

        mainTimer = new javax.swing.Timer(1000 / 60, al);
        mainTimer.start();
    }

    private void ziechneStartScreen() {
        if (!start) {
            fenster.fuelleRechteck(0, 0, breite, hoehe, new Color(25, 139, 41));
            zeichneStrasseFuss();
            zeichneStrasseAuto();
        }
    }

    private void zeichne() {
        fenster.fuelleRechteck(0, 0, breite, hoehe, new Color(25, 139, 41));
        zeichneStrasseFuss();
        zeichneStrasseAuto();

        for (Fussgaenger fussgaenger : fussgaengerArrayU) {
            fussgaenger.zeichne(fenster);
            if (!fussgaenger.stop) {
                fussgaenger.bewege();
            }
        }

        for (Fussgaenger fussgaenger : fussgaengerArrayO) {
            fussgaenger.zeichne(fenster);
            if (!fussgaenger.stop) {
                fussgaenger.bewege();
            }
        }

        for (Auto auto : autoArrayL) {
            auto.zeichne(fenster);
            if (!auto.stop) {
                auto.bewege();
            }
        }

        for (Auto auto : autoArrayR) {
            auto.zeichne(fenster);
            if (!auto.stop) {
                auto.bewege();
            }
        }

        ampelA1.zeichne(fenster);
        ampelA2.zeichne(fenster);
        ampelF1.zeichne(fenster);
        ampelF2.zeichne(fenster);

    }

    private void erzeugeAutos(int nr) {
        int x1, x2;
        int index1 = 0, index2 = 0;
        for (int i = 0; i < nr; i++) {

            if (autoArrayR.size() != 0) index1 = autoArrayR.size();
            if (autoArrayL.size() != 0) index2 = autoArrayL.size();

            int random1 = random.nextInt(7) + 1;
            int random2 = 0;

            switch (random1) {
                case 1:
                    random2 = 9;
                    break;
                case 2:
                    random2 = 10;
                    break;
                case 3:
                    random2 = 11;
                    break;
                case 4:
                    random2 = 12;
                    break;
                case 5:
                    random2 = 13;
                    break;
                case 6:
                    random2 = 14;
                    break;
                case 7:
                    random2 = 8;
                    break;
            }

            if (i % 2 == 0) {
                if (index1 > 0) x1 = autoArrayR.get(index1 - 1).getBackX() - abstandZwichenAutos;
                else x1 = 0;
                autoArrayR.add(new Auto(x1 - 60 - random.nextInt(150), hoehe / 2 + 20, random1, random2));
                autoArrayR.get(index1).richtung = 0;
                index1++;

            } else {
                if (index2 > 0) x2 = autoArrayL.get(index2 - 1).getBackX() + abstandZwichenAutos;
                else x2 = breite;
                autoArrayL.add(new Auto(x2 + random.nextInt(150), hoehe / 2 - 60, random1, random2));
                autoArrayL.get(index2).richtung = 1;
                index2++;
            }
        }
    }

    private void erzeugeMenschen(int nr, boolean zusaetzlich) {
        if (!zusaetzlich) {
            MenschenO = 0;
            MenschenU = 0;
        }

        int index1 = 0, index2 = 0;
        if (fussgaengerArrayO.size() != 0) index1 = fussgaengerArrayO.size();
        if (fussgaengerArrayU.size() != 0) index2 = fussgaengerArrayU.size();

        for (int i = 0; i < nr; i++) {
            if (i % 2 == 0) {
                fussgaengerArrayO.add(new Fussgaenger(450 - (random.nextInt(101)), 800 + random.nextInt(100), random.nextInt(7) + 1));
                fussgaengerArrayO.get(index1).richtung = 0;
                index1++;
            } else {
                fussgaengerArrayU.add(new Fussgaenger(450 - (random.nextInt(101)), 0 - random.nextInt(100), random.nextInt(7) + 1));
                fussgaengerArrayU.get(index2).richtung = 1;
                index2++;
            }
        }
        if (!zusaetzlich) {
            MenschenO = fussgaengerArrayO.size();
            MenschenU = fussgaengerArrayU.size();
        }

    }

    private void zeichneStrasseAuto() {
        fenster.fuelleRechteck(0, hoehe / 2 - (150 / 2), breite, 150, "schwarz");
        int x1 = 0, x2 = 50;
        for (int i = 0; i < 9; i++) {

            fenster.fuelleRechteck((breite / 2) - 25, hoehe / 2 - (12 / 2), 50, 12, "weiss");

            if (i <= 4) {
                fenster.fuelleRechteck((breite / 2) - 25 - x1, hoehe / 2 - (12 / 2), 50, 12, "weiss");
                x1 += 100;
            }

            if (i >= 5) {
                fenster.fuelleRechteck((breite / 2) + 25 + x2, hoehe / 2 - (12 / 2), 50, 12, "weiss");
                x2 += 100;
            }
        }

        fenster.fuelleRechteck(breite / 2 - (100 / 2) - 4, hoehe / 2 + 20, 4, 110 / 2, "weiss");
        fenster.fuelleRechteck(breite / 2 + (100 / 2), hoehe / 2 - 20 - 110 / 2, 4, 110 / 2, "weiss");
    }

    private void zeichneStrasseFuss() {
        fenster.fuelleRechteck(breite / 2 - (100 / 2), 0, 100, hoehe, "braun");
    }

    private void pruefeAmpel() { //Name ist meh!

        for (Auto autoR : autoArrayR) {
            if (autoR.getFrontX() >= breite / 2 - 70
                    && autoR.getFrontX() <= breite / 2 - 60 && ampelA1.sollAnhalten()) {
                autoR.stop = true;
                Auto temp = autoArrayR.get(autoArrayR.indexOf(autoR) + 1);
                temp.prufeAbstand(autoR, autoArrayR, abstandZwichenAutos);
            } else if (!ampelA1.sollAnhalten() && autoR.stop) autoR.stop = false;
        }


        for (Auto autoL : autoArrayL) {
            if (autoL.getFrontX() <= breite / 2 + 70
                    && autoL.getFrontX() >= breite / 2 + 60 && ampelA2.sollAnhalten()) {
                autoL.stop = true;
                Auto temp = autoArrayL.get(autoArrayL.indexOf(autoL) + 1);
                temp.prufeAbstand(autoL, autoArrayL, abstandZwichenAutos);
            } else if (!ampelA2.sollAnhalten() && autoL.stop) autoL.stop = false;

        }

        for (Fussgaenger fussgaenger : fussgaengerArrayO) {
            if (fussgaenger.getY() >= 485 + 12 && fussgaenger.getY() <= 485 + 20 && ampelF1.sollAnhalten()) {
                fussgaenger.stop = true;
                if (!ampelF1.wurdeBenutzt) {
                    ampelF1.wurdeBenutzt = true;
                    ampelF2.wurdeBenutzt = true;
                    wechsleAmpel(0);
                }

            } else if (!ampelF1.sollAnhalten() && fussgaenger.stop) fussgaenger.stop = false;
        }

        for (Fussgaenger fussgaenger : fussgaengerArrayU) {

            if (fussgaenger.getY() <= 400 - 85 - 12 && fussgaenger.getY() >= 400 - 85 - 20 && ampelF2.sollAnhalten()) {
                fussgaenger.stop = true;
                if (!ampelF2.wurdeBenutzt) {
                    ampelF2.wurdeBenutzt = true;
                    ampelF1.wurdeBenutzt = true;
                    wechsleAmpel(0);
                }

            } else if (!ampelF2.sollAnhalten() && fussgaenger.stop) fussgaenger.stop = false;
        }
    }

    private void pruefeAusserhalb() {

        for (int i = 0; i < autoArrayR.size(); i++) {
            if (autoArrayR.get(i).getBackX() >= breite) {
                autoArrayR.remove(i);
                erzeugeErneut(0);
            }
        }

        for (int i = 0; i < autoArrayL.size(); i++) {
            if (autoArrayL.get(i).getBackX() <= 0) {
                autoArrayL.remove(i);
                erzeugeErneut(1);
            }
        }

        for (int i = 0; i < fussgaengerArrayO.size(); i++) {
            if (fussgaengerArrayO.get(i).getY() - 5 <= 0) {
                fussgaengerArrayO.remove(i);
                MeschenOGeloescht++;
                erzeugeErneut(2);

            }
        }

        for (int i = 0; i < fussgaengerArrayU.size(); i++) {
            if (fussgaengerArrayU.get(i).getY() + 5 >= hoehe) {
                fussgaengerArrayU.remove(i);
                MeschenUGeloescht++;
                erzeugeErneut(2);

            }
        }
    }

    /**
     * @param whatToSpawn 0 autoR 1 autoL 2fussgaenger
     */
    private void erzeugeErneut(int whatToSpawn) {
        int random1 = random.nextInt(7) + 1;
        int random2 = 0;

        switch (random1) {
            case 1:
                random2 = 9;
                break;
            case 2:
                random2 = 10;
                break;
            case 3:
                random2 = 11;
                break;
            case 4:
                random2 = 12;
                break;
            case 5:
                random2 = 13;
                break;
            case 6:
                random2 = 14;
                break;
            case 7:
                random2 = 8;
                break;
        }

        if ((whatToSpawn == 2) && (MenschenU + MenschenO == MeschenOGeloescht + MeschenUGeloescht)) {
            erzeugeMenschen(random.nextInt(25) + 1, false);
            MeschenOGeloescht = 0;
            MeschenUGeloescht = 0;


        }

        if (whatToSpawn == 0) {
            int x1 = autoArrayR.get(autoArrayR.size() - 1).getBackX() - 60;
            if (x1 > 0) x1 = 0 - abstandZwichenAutos - random.nextInt(150);
            autoArrayR.add(new Auto(x1 - abstandZwichenAutos - random.nextInt(150), hoehe / 2 + 20, random1, random2));
            autoArrayR.get(autoArrayR.size() - 1).richtung = 0;
        }

        if (whatToSpawn == 1) {
            int x1 = autoArrayL.get(autoArrayL.size() - 1).getBackX();
            if (x1 < breite) x1 = breite + abstandZwichenAutos + random.nextInt(150);
            autoArrayL.add(new Auto(x1 + abstandZwichenAutos + random.nextInt(150), hoehe / 2 - 60, random1, random2));
            autoArrayL.get(autoArrayL.size() - 1).richtung = 1;
        }
    }

    /**
     * @param state 0: temp1 1: temp2 2: temp3 3: temp4 4: temp5
     */
    private void wechsleAmpel(int state) {

        switch (state) {
            case 0:
                TimerTask temp1 = new TimerTask() {
                    @Override
                    public void run() {
                        ampelA1.state = 1;
                        ampelA2.state = 1;
                        ampelF1.state = 2;
                        ampelF2.state = 2;
                        wechsleAmpel(1);
                    }
                };
                timerUtil.schedule(temp1, (long) 500);
                break;
            case 1:
                TimerTask temp2 = new TimerTask() {
                    @Override
                    public void run() {
                        ampelA1.state = 0;
                        ampelA2.state = 0;
                        ampelF1.state = 2;
                        ampelF2.state = 2;
                        wechsleAmpel(2);
                    }
                };
                timerUtil.schedule(temp2, (long) intervalAmpel * 1000);
                break;
            case 2:
                TimerTask temp3 = new TimerTask() {
                    @Override
                    public void run() {
                        ampelF1.state = 1;
                        ampelF2.state = 1;
                        wechsleAmpel(3);
                    }
                };
                timerUtil.schedule(temp3, (long) intervalAmpel * 1000 + 500);
                break;
            case 3:
                TimerTask temp4 = new TimerTask() {
                    @Override
                    public void run() {
                        ampelA1.state = 1;
                        ampelA2.state = 1;
                        wechsleAmpel(4);
                    }
                };
                timerUtil.schedule(temp4, (long) 500);
                break;
            case 4:
                TimerTask temp5 = new TimerTask() {
                    @Override
                    public void run() {
                        ampelF1.state = 0;
                        ampelF2.state = 0;
                        ampelA1.state = 2;
                        ampelA2.state = 2;
                        ampelF1.wurdeBenutzt = false;
                        ampelF2.wurdeBenutzt = false;
                    }
                };
                timerUtil.schedule(temp5, (long) intervalAmpel * 1000);
                break;
        }
    }
}
