/**
 * @author Raul Simpetru
 */
public class AmpelF extends Ampel {
    //update
    public int state = 0; //0 = rot, 1 = gruen , 2 == gruen aber die Fussgaenger muessen warten
    private final int x, y;
    public boolean wurdeBenutzt;

    public AmpelF(int _x, int _y) {
        super();
        x = _x;
        y = _y;
    }

    @Override
    public void zeichne(Zeichenfenster fenster) {
        fenster.fuelleRechteck(x, y, 30, 54, "grau");
        wechsleLicht(fenster);
    }

    @Override
    protected void wechsleLicht(Zeichenfenster fenster) {
        switch (state) {
            case 0:
                fenster.fuelleKreis(x + 15, y + 54 / 2 - 13, 12, "rot");
                fenster.fuelleKreis(x + 15, y + 54 / 2 + 13, 12, "schwarz");
                break;
            case 1:
                fenster.fuelleKreis(x + 15, y + 54 / 2 - 13, 12, "schwarz");
                fenster.fuelleKreis(x + 15, y + 54 / 2 + 13, 12, "gruen");
                break;
            case 2:
                fenster.fuelleKreis(x + 15, y + 54 / 2 - 13, 12, "schwarz");
                fenster.fuelleKreis(x + 15, y + 54 / 2 + 13, 12, "gruen");
                break;
        }
    }

    @Override
    public boolean sollAnhalten() {
        return state == 0 || state == 2;
    }

}
