import java.util.Random;

/**
 * @author Raul Simpetru
 */

public class Fussgaenger {

    public int richtung; // 0 = geht nach oben; 1 = geht nach unten
    private final int x, farbe;
    private int y;
    public boolean stop;
    private double speed;

    public Fussgaenger(int _x, int _y, int _farbe) {
        x = _x;
        y = _y;
        farbe = _farbe;
        stop = false;
        Random random = new Random();
        speed = random.nextInt(10 + 10) / 10;
        while (speed == 0) speed = random.nextInt(20) / 10;
    }

    public void zeichne(Zeichenfenster fenster) {
        fenster.fuelleKreis(x, y, 10, farbe);
        if (richtung == 0) {
            fenster.fuelleKreis(x + 10, y - 4, 5, farbe);
            fenster.fuelleKreis(x - 10, y - 4, 5, farbe);
        }
        if (richtung == 1) {
            fenster.fuelleKreis(x + 10, y + 4, 5, farbe);
            fenster.fuelleKreis(x - 10, y + 4, 5, farbe);
        }
    }

    public void bewege() {
        if (!stop) {
            if (richtung == 0) y -= (1 + speed);
            else if (richtung == 1) y += 1 + speed;
        }
    }

    public int getY() {
        return y;
    }

}