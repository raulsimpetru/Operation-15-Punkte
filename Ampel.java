/**
 * @author Raul Simpetru
 */

public abstract class Ampel {

    public Ampel() {
    }

    protected abstract void zeichne(Zeichenfenster fenster);

    protected abstract void wechsleLicht(Zeichenfenster fenster);

    protected abstract boolean sollAnhalten();
}
